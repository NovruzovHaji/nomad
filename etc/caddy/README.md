# TCP with Caddy

https://github.com/mholt/caddy-l4
( from https://caddy.community/t/reverse-proxy-any-tcp-connection-for-database-connections/12732/2 )

[tcp.ctmpl](tcp.ctmpl) -- verify by running [echo.js](echo.js)

@see https://git.archive.org/www/ci/-/tree/echo.js sample deploy that used TCP
